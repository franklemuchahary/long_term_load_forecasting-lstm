
# coding: utf-8

# In[261]:


#import required libraries
import pandas as pd
import numpy as np
from scipy import stats
import re
from datetime import datetime, timedelta
import calendar
from collections import Counter, OrderedDict
import pickle
import json


#plotting
#import seaborn as sns
import matplotlib.pyplot as plt
get_ipython().magic('matplotlib inline')
from pylab import rcParams
rcParams['figure.figsize'] = 15, 10


# ## Read CSV Files 
# Dataset for 2004-2015

# In[128]:


#specify the columns to use
cols_use = ['Date', 'Hour', 'DA_DEMD', 'DryBulb', 'DewPnt']


# In[352]:


global tot_data

def read_csv_files(file_path, columns, year_range_end):
    list_of_dfs = []   

    for i in range(4,year_range_end):
        if(i<10):
            f_name = file_path[0:3]+str(i)+file_path[4:]
        else:
            f_name = file_path[0:2]+str(i)+file_path[4:]
        try:   
            if(i==4 or i==8 or i==12): #for the case of leap year
                temp_df = pd.read_csv(f_name, usecols=cols_use, skiprows=range(1417,1441))
                list_of_dfs.append(temp_df)
            else: #for the other years
                temp_df = pd.read_csv(f_name, usecols=cols_use)
                list_of_dfs.append(temp_df)
            print("Done. File Name: ", f_name, temp_df.shape)
        except Exception as e:
            print(e)
            
    return list_of_dfs


def append_dataframes(df_list):
    tot_data = df_list[0].append(df_list[1], ignore_index=True)

    for i in df_list[2:]:
        tot_data = tot_data.append(i, ignore_index=True)
    
    print(tot_data.shape)
    return tot_data


def load_files_rolling(name_file, year_range_end, new_preds=None):
    df_list = read_csv_files(name_file_one, cols_use, year_range_end)
    if(new_preds!=None):
        df_list[-2]['DA_DEMD'] = new_preds
    tot_data = append_dataframes(df_list)
    return tot_data
    


name_file_one = '2004.csv'

tot_data = load_files_rolling(name_file_one, 16, list(preds_dict[3]['Predicted']))


# #### Get Features from dates

# In[353]:


#convert the date string to datetime obj
def convert_string_to_datetime(string):
    datetime_obj = datetime.strptime(string, "%m/%d/%Y")
    return datetime_obj.date()

tot_data['Date'] = tot_data['Date'].apply(convert_string_to_datetime)
tot_data['Date'] = pd.to_datetime(tot_data['Date'])

#get weekday from date
def get_day_of_week(date_obj):
    return date_obj.weekday()

tot_data['Day'] = tot_data['Date'].apply(get_day_of_week)

def get_year(date_obj):
    return date_obj.year

def get_month(date_obj):
    return date_obj.month

tot_data['Year'] = tot_data['Date'].apply(get_year)
tot_data['Month'] = tot_data['Date'].apply(get_month)


# #### Fill Missing Values

# In[354]:


missing_vals = ['DA_DEMD']

for j in missing_vals:
    for i in range(len(tot_data[j])):
        if(tot_data[j][i] < 1):
            tot_data[j][i] = (tot_data[j][i-1]+tot_data[j][i+1])/2
    print(j)


# ### Create Labels
labels_DA_DEMD = tot_data[8760:]['DEMAND']
labels_DA_DEMD = labels_DA_DEMD.reset_index(drop=True)tot_data = tot_data[:-8760]
#add labels to main dataset
tot_data['TARGET_DA_DEMD'] = labels_DA_DEMD
tot_data.shape
# #### Prepare Data for RNN

# In[294]:


def concat_date_time_to_timestamp(df):
    #print(df['Date'], df['Hour'])
    return pd.Timestamp(df['Date'] + " "+ str(df['Hour']-1) + ":" + "00")

def give_rnn_data(data, date_train_string="31-12-2010", date_test_string="01-01-2011"):
    # data['Date'] = data[['Date', 'Hour']].apply(concat_date_time_to_timestamp, axis=1)
    tot_data_ts  = data[['Date', 'DA_DEMD', 'Hour', 'Year', 'Month', 'Day']]
    #tot_data_ts['Date'] = tot_data_ts['Date'].apply(convert_string_to_datetime)

    tot_data_ts = tot_data_ts.set_index(pd.DatetimeIndex(tot_data_ts['Date']))
    tot_data_ts = tot_data_ts.drop('Date', axis=1)

    for s in range(8761-24, 8761):
        tot_data_ts['DEMAND_shift_{}'.format(s)] = tot_data_ts['DA_DEMD'].shift(s)
        tot_data_ts['Hour_shift_{}'.format(s)] = tot_data_ts['Hour'].shift(s)
        tot_data_ts['Year_shift_{}'.format(s)] = tot_data_ts['Year'].shift(s)
        tot_data_ts['Month_shift_{}'.format(s)] = tot_data_ts['Month'].shift(s)
        tot_data_ts['Day_shift_{}'.format(s)] = tot_data_ts['Day'].shift(s)
        #tot_data_ts['DryB_shift_{}'.format(s)] = tot_data_ts['DryBulb'].shift(s)
        #print(s)
        
    tot_data_ts = tot_data_ts.drop(['Hour', 'Year', 'Month', 'Day'], axis=1)    
    X = tot_data_ts.dropna().drop('DA_DEMD', axis=1)
    Y = tot_data_ts.dropna()[['DA_DEMD']]

    train_x = X[:pd.Timestamp(date_train_string)]
    test_x = X[pd.Timestamp(date_test_string):]

    train_y = Y[:pd.Timestamp(date_train_string)]
    test_y = Y[pd.Timestamp(date_test_string):]
    
    train_x = train_x.values
    test_x = test_x.values

    train_y = train_y.values
    test_y = test_y.values
    
    from sklearn import preprocessing
    std_scaler2 = preprocessing.StandardScaler()
    train_x_scaled = std_scaler2.fit_transform(train_x)
    test_x_scaled = std_scaler2.fit_transform(test_x)
    train_y_scaled = std_scaler2.fit_transform(train_y)
    
    return(train_x_scaled, test_x_scaled, train_y_scaled, test_y, std_scaler2)


# ### Deal With Missing DEMAND and LMP  values
sns.distplot(tot_data['TARGET_DA_DEMD'])
# ## Feature Engineering

# #### Break Down Date into Further Features

# ##### Get Daily Mean Values of DEMAND, LMP, Temp and Dew Pnt
mean_values = tot_data.groupby('Date', as_index=False).mean()[['Date','DA_DEMD', 'DryBulb', 'DewPnt']]
temp = pd.merge(tot_data, mean_values, how='inner', left_on='Date', right_on='Date', 
                suffixes=('_','_daily_mean'))
tot_data = temp
# ##### Get Daily Max and Min Values of Temp and Dew Pnt
max_values = tot_data.groupby('Date', as_index=False).max()[['Date','DryBulb_', 'DewPnt_']]
temp = pd.merge(tot_data, max_values, how='inner', left_on='Date', right_on='Date', 
                suffixes=('_','_daily_max'))
tot_data = temp

min_values = tot_data.groupby('Date', as_index=False).min()[['Date','DryBulb__', 'DewPnt__']]
temp = pd.merge(tot_data, min_values, how='inner', left_on='Date', right_on='Date', 
                suffixes=('_','_daily_min'))
tot_data = temp
# ##### Add Crude Oil and Natural Gas Data

# 

# ## Feature Selection and Scaling
# 
# Create Separate Data sets for **Load** and **Price**
from sklearn.preprocessing import StandardScaler
# In[45]:


tot_data.columns


# ##### Demand Prediction Dataset
load_data = tot_data[['TARGET_DA_DEMD', 'Hour', 'DA_DEMD_','Day', 'Year', 'Month', 'DA_DEMD_daily_mean', 
                     ]]

load_train = load_data[load_data['Year'] < 2013]
load_train = load_train.reset_index(drop=True)

load_test = load_data[load_data['Year'] >= 2013]
load_test = load_test.reset_index(drop=True)load_train_x = load_train.iloc[:, 1:]
load_train_y = load_train['TARGET_DA_DEMD']

load_test_x = load_test.iloc[:, 1:]
load_test_y = load_test['TARGET_DA_DEMD']std_scaler = StandardScaler()
load_train_x_scaled = std_scaler.fit_transform(load_train_x)
load_test_x_scaled = std_scaler.fit_transform(load_test_x)
load_train_y_scaled = std_scaler.fit_transform(load_train_y)
# # Model Building

# #### Metrics

# In[93]:


#metrics
from sklearn.metrics import mean_absolute_error
from sklearn.metrics import mean_squared_error

#function to calculate mape
def mean_absolute_percentage_error(y_pred, y_true): 
    return np.mean(np.abs((y_true - y_pred) / y_true)) * 100

def point_absolute_percentage_error(y_pred, y_true):
    return np.abs((y_true - y_pred) / y_true) * 100

#GIVE Everything you can imagine of function - SD, VAR, MIN and MAX ERROR, CI
def get_CI_VAR_SD(error_list):
    t = stats.t.ppf(1-0.025, len(error_list)-1)
    max_err = np.mean(error_list) + (t * (np.std(error_list)/np.sqrt(len(error_list))))
    min_err = np.mean(error_list) - (t * (np.std(error_list)/np.sqrt(len(error_list))))
    ci = ((max_err - np.mean(error_list))/np.mean(error_list))*100
    sd = np.std(error_list)
    var = np.var(error_list)
    return (max_err, min_err, ci, sd, var)


def get_scoring(model, test_file, train_file, load_test_y, load_train_y, scaler, load=True, metric='mape', 
                keras_nn=False):
    train_preds = model.predict(train_file)
    if(keras_nn):
        train_preds = train_preds.flatten()
    train_preds = scaler.inverse_transform(train_preds)
    
    test_preds = model.predict(test_file)
    if(keras_nn):
        test_preds = test_preds.flatten()
    test_preds = scaler.inverse_transform(test_preds)
    
    if(load):
        if(metric == 'mape'):
            err_train = mean_absolute_percentage_error(train_preds, load_train_y)
            err_test = mean_absolute_percentage_error(test_preds, load_test_y)
    
        if(metric == 'mae'):
            err_train = mean_absolute_error(train_preds, load_train_y)
            err_test = mean_absolute_error(test_preds, load_test_y)
            
        if(metric == 'rmse'):
            err_train = np.sqrt(mean_squared_error(train_preds, load_train_y))
            err_test = np.sqrt(mean_squared_error(test_preds, load_test_y))
            
    return(err_test, test_preds, err_train, train_preds)



def get_monthly_scores(test_preds, test_y, metric='mape'):
    days_list = [0,31,28,31,30,31,30,31,31,30,31,30,31]
    score_dict = OrderedDict()
    
    for i in range(0,len(days_list)-1):
        mnth = calendar.month_abbr[i+1]
        if(metric=='mape'):
            score_dict[mnth] = mean_absolute_percentage_error(
                            test_preds[int(np.sum(days_list[:i+1]))*24:int(np.sum(days_list[:i+2]))*24], 
                            test_y[int(np.sum(days_list[:i+1]))*24:int(np.sum(days_list[:i+2]))*24])
    return score_dict  


# ##### Load Models

# In[61]:


#NN Models
from keras.models import Sequential
from keras.layers import Dense
import keras.backend as K
from keras.callbacks import EarlyStopping
from keras.layers import LSTM
from keras.layers import Dropout
from keras.optimizers import SGD
from keras.models import model_from_json


# In[160]:


#Other Models 
from sklearn.ensemble import RandomForestRegressor
from xgboost import XGBRegressor
from sklearn.grid_search import GridSearchCV


# ##### Save Model to Disk

# In[187]:


def save_model(model_obj, filename):
    try:
        with open(filename, 'wb') as fid:
            pickle.dump(model_obj, fid) 
        print("Done")
    except Exception as e: print(e)
        
        
def save_keras_model_to_json(model, filename):
    try:
        json_arch = model.to_json()
        with open(filename, 'w') as outfile:
            json.dump(json_arch, outfile)
        print("Done")
    except Exception as e: print(e)
    '''
    model reconstruction from JSON:
    model = model_from_json(json_string)
    '''
    
def read_keras_model_from_json(filename):
    file = open(filename, 'r')
    model_json_str = file.read()
    file.close()
    model = model_from_json(model_json_str)
    return model_json_str


# ### Demand Prediction

# ### LSTM
load_rnn_train_x = load_train_x_scaled.reshape(load_train_x_scaled.shape[0],1,6)
load_rnn_test_x = load_test_x_scaled.reshape(load_test_x_scaled.shape[0],1,6)

early_stop = EarlyStopping(monitor='loss', patience=1, verbose=1)

print(load_rnn_train_x.shape)
# In[ ]:


preds_dict = {}


# #### if time series dataset

# In[346]:


load_train_x_scaled, load_test_x_scaled, load_train_y_scaled, load_test_y, scaler = give_rnn_data(tot_data,
                                                                                    date_train_string="31-12-2014", 
                                                                                    date_test_string="01-01-2015")
load_rnn_train_x = load_train_x_scaled.reshape(load_train_x_scaled.shape[0],24,5)
load_rnn_test_x = load_test_x_scaled.reshape(load_test_x_scaled.shape[0],24,5)

early_stop = EarlyStopping(monitor='loss', patience=1, verbose=1)

print(load_rnn_train_x.shape)


# In[355]:


K.clear_session()
lstm_model = Sequential()

lstm_model.add(LSTM(15, input_shape=(24, 5), activation='relu', return_sequences=True))
lstm_model.add(LSTM(15, activation='relu', return_sequences=True))
lstm_model.add(LSTM(15, activation='relu'))
lstm_model.add(Dense(1))

sgd = SGD(lr=0.01, decay=1e-6, momentum=0.9, nesterov=True)
lstm_model.compile(loss='mean_absolute_error', optimizer=sgd)

lstm_model.fit(load_rnn_train_x, load_train_y_scaled,
          epochs=15, batch_size=300, verbose=1,
         callbacks=[early_stop], validation_split=0.1)


# In[357]:


load_train_y = scaler.inverse_transform(load_train_y_scaled).flatten()
load_test_y = load_test_y.flatten()
lstm_test_err, lstm_test_preds, lstm_train_err, lstm_train_preds = get_scoring(lstm_model, load_rnn_test_x, 
                                                                            load_rnn_train_x, load_test_y,
                                                                            load_train_y, scaler, 
                                                                            True, 'mape', True)

year_preds = {}
year_preds['Monthly'] = get_monthly_scores(lstm_test_preds, load_test_y)
year_preds['True'] = load_test_y
year_preds['Predicted'] = lstm_test_preds
year_preds['MAPE_TEST'] = lstm_test_err

preds_dict[4] = year_preds

print(lstm_test_err, lstm_train_err)


# In[358]:


preds_dict


# In[375]:


output = open('output.txt', 'ab+')

pickle.dump(preds_dict, output)
output.close()


# In[376]:


output = open('output.txt', 'rb')
obj_dict = pickle.load(output)  


# In[252]:


lstm_model.summary()


# In[183]:


save_keras_model_to_json(lstm_model, 'model_lstm_json.txt')


# ### MLP
K.clear_session()
 
mlp_model = Sequential()
mlp_model.add(Dense(15, input_shape=(14,), activation='relu'))
mlp_model.add(Dense(10, activation='relu'))
mlp_model.add(Dense(3))
mlp_model.add(Dense(1))

sgd = SGD(lr=0.01, decay=1e-6, momentum=0.9, nesterov=True)
mlp_model.compile(loss='mean_absolute_error', optimizer=sgd)

mlp_model.fit(load_train_x_scaled, load_train_y_scaled,
          epochs=20, batch_size=50, verbose=1,
         callbacks=[early_stop], validation_split=0.1)mlp_test_err, mlp_test_preds, mlp_train_err, mlp_train_preds = get_scoring(mlp_model, load_test_x_scaled, 
                                                                             load_train_x_scaled, std_scaler, 
                                                                             True, 'mape', True)
print(mlp_test_err, mlp_train_err)

get_monthly_scores(mlp_test_preds, load_test_y)save_keras_model_to_json(mlp_model, 'model_mlp_json.txt')
# ### XGBoost
params = {
    'max_depth': [5],
    'min_child_weight':[7],
    'n_estimators': [800],
    'learning_rate':[0.01],
    'reg_lambda':[3],
    'gamma': [0.01],
    'reg_alpha':[1e-5]
}

xgb = GridSearchCV(XGBRegressor(silent=0), params, n_jobs=-1, cv=10, scoring='neg_mean_squared_error') 
#xgb = XGBRegressor(silent=0, max_depth=7, min_child_weight=14, n_estimators=800, learning_rate=0.01,
                            #reg_lambda=3, gamma=0.01, reg_alpha=1e-5)

xgb.fit(load_train_x_scaled, load_train_y_scaled)

try:
    print(xgb.best_params_)
    print("\n")
    print(xgb.best_score_)
except:
    print("Did Not Use GridSearch")xgb_test_err, xgb_test_preds, xgb_train_err, xgb_train_preds = get_scoring(xgb, load_test_x_scaled, 
                                                                             load_train_x_scaled, std_scaler, 
                                                                             True, 'mape', True)
print(xgb_test_err, xgb_train_err)

save_model(xgb, 'model_xgb.pk')

get_monthly_scores(xgb_test_preds, load_test_y)
# # Plots

# In[421]:


#import matplotlib.pyplot as plt
temp = pd.DataFrame({
    'Proposed':preds_dict[0]['Predicted'][:-24], 
    'Actual':preds_dict[0]['True'][:-24]   
})
temp = temp.reset_index(drop=True)
plt.plot(temp['Proposed'], 'r--', label="Predicted")
plt.plot(temp['Actual'], 'g-', label="Actual")
plt.xlabel("Hour", {'fontsize':30})
plt.ylabel("Load Demand",{'fontsize':30})
plt.xticks(fontsize=20, rotation=0)
plt.yticks(fontsize=20, rotation=0)
plt.title("Predicted vs Actual Load", {'fontsize':30})
plt.legend(loc=2, fontsize=23)
#plt.show()
#plt.gcf()
plt.savefig("plot_single_year_1st_year.jpg", format='jpg', bbox_inches='tight', dpi=600)


# In[422]:


#import matplotlib.pyplot as plt
temp = pd.DataFrame({
    'Proposed':preds_dict[4]['Predicted'][:-24], 
    'Actual':preds_dict[4]['True'][:-24]   
})
temp = temp.reset_index(drop=True)
plt.plot(temp['Proposed'], 'r--', label="Predicted")
plt.plot(temp['Actual'], 'g-', label="Actual")
plt.xlabel("Hour", {'fontsize':30})
plt.ylabel("Load Demand",{'fontsize':30})
plt.xticks(fontsize=20, rotation=0)
plt.yticks(fontsize=20, rotation=0)
plt.title("Predicted vs Actual Load", {'fontsize':30})
plt.legend(loc=2, fontsize=23)
#plt.show()
#plt.gcf()
plt.savefig("plot_single_year_5th_year.jpg", format='jpg', bbox_inches='tight', dpi=600)


# In[423]:


#import matplotlib.pyplot as plt
temp = pd.DataFrame({
    'Proposed':preds_dict[0]['Predicted'][2160:2880], 
    'Actual':preds_dict[0]['True'][2160:2880]   
})
temp = temp.reset_index(drop=True)
plt.plot(temp['Proposed'], 'r--', label="Predicted")
plt.plot(temp['Actual'], 'g-', label="Actual")
plt.xlabel("Hour", {'fontsize':30})
plt.ylabel("Load Demand",{'fontsize':30})
plt.xticks(fontsize=20, rotation=0)
plt.yticks(fontsize=20, rotation=0)
plt.title("Predicted vs Actual Load (April, 2011)", {'fontsize':30})
plt.legend(loc=1, fontsize=23)
#plt.legend(bbox_to_anchor=(1, 1), loc=2, borderaxespad=0.1, fontsize=18)
#plt.show()
#plt.gcf()
plt.savefig("plot_single_month_1st_year.jpg", format='jpg', bbox_inches='tight', dpi=600)


# In[424]:


#import matplotlib.pyplot as plt
temp = pd.DataFrame({
    'Proposed':preds_dict[0]['Predicted'][2160+168:2160+168+168], 
    'Actual':preds_dict[0]['True'][2160+168:2160+168+168]   
})
temp = temp.reset_index(drop=True)
plt.plot(temp['Proposed'], 'r--', label="Predicted")
plt.plot(temp['Actual'], 'g-', label="Actual")
plt.xlabel("Hour", {'fontsize':30})
plt.ylabel("Load Demand",{'fontsize':30})
plt.xticks(fontsize=20, rotation=0)
plt.yticks(fontsize=20, rotation=0)
plt.title("Predicted vs Actual Load (One Week)", {'fontsize':30})
plt.legend(loc=2, fontsize=23)
#plt.legend(bbox_to_anchor=(1, 1), loc=2, borderaxespad=0.1, fontsize=18)
#plt.show()
#plt.gcf()
plt.savefig("plot_single_week_1st_year.jpg", format='jpg', bbox_inches='tight', dpi=600)


# In[418]:


abcdef = pd.DataFrame([j for i,j in preds_dict[0]['Monthly'].items()])
abcdef = abcdef.transpose()
my_plt = abcdef.mean().plot(kind='bar')
plt.xlabel("Months", {'fontsize':30})
plt.ylabel("Mean Absolute Percentage Errors",{'fontsize':30})
plt.xticks(fontsize=20, rotation=0)
plt.yticks(fontsize=20, rotation=0)
my_plt.set_xticklabels([i for i,j in preds_dict[0]['Monthly'].items()])
plt.title("Monthly MAPE (2011)", {'fontsize':30})
plt.savefig("plot_monthly_mape_1st_year.jpg", format='jpg', bbox_inches='tight', dpi=600)


# In[425]:


plt.plot(tot_data['DA_DEMD'][:8760])
plt.xlabel("Time(Hours)", {'fontsize':30})
plt.ylabel("Electricity Load Demand",{'fontsize':30})
plt.xticks(fontsize=20, rotation=0)
plt.yticks(fontsize=20, rotation=0)
plt.title("Electricity Load Demand Raw Data (2004)", {'fontsize':30})
plt.savefig("plot_electricity_demands.jpg", format='jpg', bbox_inches='tight', dpi=600)


# In[427]:


get_ipython().system('nvidia-smi')

